# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

# function to find the manhattan distance from a point to a list of point and return
# the nearest point and also returning the position of the nearest node
def minCostOfAllFood(state,listCor):
    if type(listCor) is None:
        return
    minCostDict = {}
    for dist in listCor:
        minCostDict[dist] = util.manhattanDistance(state,dist)
    sortedDist = sorted(minCostDict.items(), key=lambda item: item[1])
    if len(sortedDist)>0:
        return sortedDist[0][0],sortedDist[0][1]
    return None

class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()

        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]
        # returning infinite score if success
        if successorGameState.isWin():
            return float("inf")
        # getting the list of ghosts to calculate position
        # assuming that the ghost moves after the pacman
        # hence taking the ghostPosition from currentGameState
        unVisGhostList = []
        for i in range(1,currentGameState.getNumAgents()):
            unVisGhostList.append(currentGameState.getGhostPosition(i))
        # calculating distance from all ghosts
        distFromAllGhost = 0
        for pos in unVisGhostList:
            distFromAllGhost += util.manhattanDistance(pos,newPos)
        # adding distance to the ghosts and score of successor
        cumVal = distFromAllGhost + successorGameState.getScore()
        # boosting if one food is to be eaten up in the successful state
        if (currentGameState.getNumFood() > successorGameState.getNumFood()):
            cumVal += 10
        # penalising if its a stop
        if action == Directions.STOP:
            cumVal -= 10
        # reducing the nearest food assuming that we will go there and the nearest
        # position will have the minimal value and reduces the least
        # hence keeping the cumulative score high
        cumVal -= minCostOfAllFood(newPos,newFood.asList())[1]
        # finally checking if the next states has power up capsules
        # for each power capsules we are rewarding with a score
        powerUp = currentGameState.getCapsules()
        if successorGameState.getPacmanPosition() in powerUp:
            cumVal += 10
        return cumVal

def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """
        return self.value(gameState,0,0)[0]
        #util.raiseNotDefined()

    # Value function - base function which will invoke minimum and maximum functions
    def value(self,state,agentInd,dep):
        if agentInd >= state.getNumAgents():
            agentInd = 0
            dep += 1
        if dep == self.depth:
            return self.evaluationFunction(state)

        # maximising the case if its a pacman and if its a new depth
        # starting from pacman's possible action and progressively minimizing the
        # value using minVal function
        if agentInd == 0:
            return self.maxVal(state,agentInd,dep)
        else:
            return self.minVal(state,agentInd,dep)

    # minimizer function
    def minVal(self,state,agentInd,dep):
        tupToRet = ("dummy",float("inf"))
        # if terminal state evaluating the state
        if not state.getLegalActions(agentInd):
            return self.evaluationFunction(state)

        # from the legal moves computin the minimum and returning the appropriate action
        for moves in state.getLegalActions(agentInd):
            if moves == "Stop":
                continue
            computedVal = self.value(state.generateSuccessor(agentInd,moves),agentInd+1,dep)
            # having the return value as a tuple in this case and integer in the other case
            # this is to keep track of the action that needs to be taken by the pacman
            # hence this check to extract the appropriate value
            if type(computedVal) is tuple:
                computedVal = computedVal[1]
            updatedVal = min(tupToRet[1],computedVal)
            if updatedVal is not tupToRet[1]:
                tupToRet = (moves,updatedVal)
        return tupToRet

    # maximiser function
    def maxVal(self,state,agentInd,dep):
        tupToRet = ("dummy",(-1*float("inf")))
        # if terminal state evaluating the state
        if not state.getLegalActions(agentInd):
            return self.evaluationFunction(state)

        # from the legal moves computin the maximum and returning the appropriate action
        for moves in state.getLegalActions(agentInd):
            if moves == "Stop":
                continue
            computedVal = self.value(state.generateSuccessor(agentInd,moves),agentInd+1,dep)
            # having the return value as a tuple in this case and integer in the other case
            # this is to keep track of the action that needs to be taken by the pacman
            # hence this check to extract the appropriate value
            if type(computedVal) is tuple:
                computedVal = computedVal[1]
            updatedVal = max(tupToRet[1],computedVal)
            if updatedVal is not tupToRet[1]:
                tupToRet = (moves,updatedVal)
        return tupToRet

class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
      Alpha Beta pruning = using alpha beta values to control the search tree exploration
      and improve searching
    """
    # initializing alpha and beta values
    beta = float("inf")
    alpha = (-1*float("inf"))
    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        # initial state is pacmanIndex that is zero
        return self.value(gameState,0,0,self.alpha,self.beta)[0]

    # returns a tuple with action and value
    def value(self,state,agentInd,dep,alpha,beta):
        # when computed for the last agent again starts from pacman for the next
        # successive tree expansion
        # alpha - maximum upper bound
        # beta - minimum lower bound
        if agentInd >= state.getNumAgents():
            agentInd = 0
            dep += 1
        if dep == self.depth:
            return self.evaluationFunction(state)
        if agentInd == 0:
            return self.maxVal(state,agentInd,dep,alpha,beta)
        else:
            return self.minVal(state,agentInd,dep,alpha,beta)

    # same minimizer function used in the minimax algorithm
    # with the extra factor of alpha and beta for controlling tree pruning
    def minVal(self,state,agentInd,dep,alpha,beta):
        tupToRet = ("dummy",float("inf"))
        if not state.getLegalActions(agentInd):
            return self.evaluationFunction(state)
        for moves in state.getLegalActions(agentInd):
            if moves == "Stop":
                continue
            computedVal = self.value(state.generateSuccessor(agentInd,moves),agentInd+1,dep,alpha,beta)
            if type(computedVal) is tuple:
                computedVal = computedVal[1]
            updatedVal = min(tupToRet[1],computedVal)
            if updatedVal is not tupToRet[1]:
                tupToRet = (moves,updatedVal)
            # if the computed value is less than alpha return the value directly
            # and not explore the remaining successors of the node
            # since alpha is the minimum upper bound
            if tupToRet[1] < alpha:
                return tupToRet
            # keep beta updated value if the value is minimal
            beta = min(beta,tupToRet[1])
        return tupToRet

    def maxVal(self,state,agentInd,dep,alpha,beta):
        tupToRet = ("dummy",(-1*float("inf")))
        if not state.getLegalActions(agentInd):
            return self.evaluationFunction(state)
        for moves in state.getLegalActions(agentInd):
            if moves == "Stop":
                continue
            computedVal = self.value(state.generateSuccessor(agentInd,moves),agentInd+1,dep,alpha,beta)
            if type(computedVal) is tuple:
                computedVal = computedVal[1]
            updatedVal = max(tupToRet[1],computedVal)
            if updatedVal is not tupToRet[1]:
                tupToRet = (moves,updatedVal)
            # if the computed value is greater than beta return the value directly
            # and not explore the remaining successors of the node
            # since alpha is the maximum lower bound
            if tupToRet[1] > beta:
                return tupToRet
            # keep alpha updated value with max value else
            alpha = max(alpha,tupToRet[1])
        return tupToRet

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        # calculate the expected value of the given state and index
        def expectedValue(state,index,depth):
            # induction case of zero depth and win lose conditions
            if depth == 0 or state.isWin() or state.isLose():
                return self.evaluationFunction(state)

            # getting all legal actions for an index
            successorStates = state.getLegalActions(index)
            # getting number of ghosts
            ghostCount = state.getNumAgents()-1
            total = 0
            for moves in successorStates:
                nextMove = state.generateSuccessor(index,moves)
                # if the case is for the last ghost then we need to compute the
                # values for the next depth hence its directed to max value
                # expected value function keeps summing up the values of the
                # terminal nodes and in this case the probability of
                # each vertex is 1 hence we are summing up and finding average
                if index == ghostCount:
                    total += maxVal(nextMove,depth-1)
                else:
                    total += expectedValue(nextMove,index+1,depth)
            return total/len(successorStates)

        def maxVal(state,depth):
            # induction case of zero depth and win lose conditions
            if depth == 0 or state.isWin() or state.isLose():
                return self.evaluationFunction(state)
            # this function handles the depth transition from one depth
            # to other hence starting over from pacman
            pacmanInd = 0
            successorStates = state.getLegalActions(pacmanInd)
            total = (-1 * float("inf"))
            # in this case we are computing the max of all possible values
            # to choose the optimal action
            for moves in successorStates:
                nextMove = state.generateSuccessor(pacmanInd,moves)
                total = max(total,expectedValue(nextMove,1,depth))
            return total

        # initiator of the Expectimax search
        if gameState.isWin() or gameState.isLose():
            return self.evaluationFunction(state)
        # initiating optimal action
        optiAct = Directions.STOP
        pointsEarned = (-1*float("inf"))
        # starting with the successors of the pacman
        for action in gameState.getLegalActions(0):
            # representing the next state based on action in nextMove
            nextMove = gameState.generateSuccessor(0,action)
            ultimatePoint = pointsEarned
            # calculating the expected value for the first ghost and it
            # recurses from there to compute for all values
            pointsEarned = max(pointsEarned,expectedValue(nextMove,1,self.depth))
            # if the points earned are higher update the action
            if pointsEarned > ultimatePoint:
                optiAct = action
        return optiAct

def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION: The code was same as the simple evaluation function
      except for the fact this takes into account about the scared ghost
      and adds weightage to capsules and food.

      The capsules are weighed more than the food to increase the chances of
      eating them.
    """
    # returning infinite score if success
    if currentGameState.isWin():
        return float("inf")
    if currentGameState.isLose():
        return (-1*float("inf"))

    unVisGhostList = []
    cumVal = 0
    # taking the ghosts that are not scared
    for i in range(1,currentGameState.getNumAgents()):
        if currentGameState.getGhostState(i).scaredTimer > 0:
            continue
        unVisGhostList.append(currentGameState.getGhostPosition(i))
    # calculating distance from all ghosts that are not scared
    distFromAllGhost = 0
    for pos in unVisGhostList:
        distFromAllGhost += util.manhattanDistance(pos,currentGameState.getPacmanPosition())
    # adding distance to the ghosts and score of currentGameState
    cumVal = -distFromAllGhost + currentGameState.getScore()
    # adding weight to the food
    cumVal -= 4*currentGameState.getNumFood()

    # reducing the nearest food assuming that we will go there and the nearest
    # position will have the minimal value and reduces the least
    # hence keeping the cumulative score high
    cumVal -= minCostOfAllFood(currentGameState.getPacmanPosition(),currentGameState.getFood().asList())[1]
    # finally checking if the next states has power up capsules
    powerUp = currentGameState.getCapsules()
    # adding more weight to the power capsule to make pacman eat it when its nearby
    cumVal -=  20*len(powerUp)
    return cumVal

# Abbreviation
better = betterEvaluationFunction
